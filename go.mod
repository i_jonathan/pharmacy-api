module Pharmacy

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/jackc/pgx/v4 v4.14.1 // indirect
	github.com/jinzhu/now v1.1.4 // indirect
	github.com/swaggo/swag v1.7.8
	golang.org/x/crypto v0.0.0-20211215153901-e495a2d5b3d3
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	gorm.io/driver/postgres v1.2.3
	gorm.io/gorm v1.22.4
)
